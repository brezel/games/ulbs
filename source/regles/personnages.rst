Équipage
========

L'équipage d'un dirigeable est caractérisé par ses deux principaux personnages : le capitaine et l'artificier.

Le premier est en charge des déplacements tandis que le deuxième s'occupe des canons, fusils et autres armes.

Caractéristiques et compétences
-------------------------------

.. note::

    Il est possible de casser les dés pour répartir au mieux. Pour celà, un dé vaut 3 points qu'il est possible de
    placer là où vous le souhaitez.

    Ex. : 9D = 3D+1, 3D+1, 1D

Caractéristiques
~~~~~~~~~~~~~~~~

.. Nombre de dés = 3 × nb caractéristiques

9D à répartir sur les caractéristiques suivantes :

Réflexes
    Temps de réaction

Coordination
    Rapidité d'execution

Perception
    Observation et détection

Compétences
~~~~~~~~~~~

Le personnage posséde 7D

.. note::

    Des compétences spécifiques aux factions sont disponibles dans les sections dédiées.

Chanceux
    En cas de 1 sur le dé de destin, le joueur peux relancer (utilisble une fois par point dépensé)