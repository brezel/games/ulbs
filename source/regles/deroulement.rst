Déroulement d'une partie
========================

Préparation
-----------

Partie
------

Chaque joueur joue chacun son tour dans l'ordre définis.

.. note::

    Si le scenario ne donne pas l'ordre de jeu, les joueurs peuvent tiré aux dés ou se mettre d'accord sur une autre
    manière de faire.

.. todo Revoir le nombre de points en fonctions des coups des différentis actions

Pour chaque tour, chaque navire posséde 5 points d'actions qu'ils peuvent utiliser comme ils le souhaitent et dans
l'ordre qu'ils souhaitent.

Cependant, tous les points d'un navire doivent être joués à la suite (si vous manipulez un autre navire, les points
restants sont perdus).

Les actions peuvents être :

- des déplacements
- des manœuvres de combat

Une fois que tous les navires ont été manipulés, ou que le joueur annonce qu'il a terminé, on passe au joueur suivant.

Victoire
--------

