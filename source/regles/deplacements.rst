Déplacements
============

Les déplacements se font sur les points de la grille comme ci dessous :

.. image:: /_static/dirigeables_deplacements.png

Cependans, un dirigeable ne peut réélement avancer que dans la direction à laquelle il fait face :

.. image:: /_static/dirigeables_deplacements_exemples.png

La distance entre 2 points et d'un quart de mile nautique (à peut près 450 mètres). C'est cette unitée qui est utilisée
sur la fiche de description des armes.

1/8 de tour
-----------

=============== ==========
Points d'action Difficulté
=============== ==========
1 point         10
=============== ==========

La manœuvre consiste à faire tourner le navire d'un cran dans le sens horaire ou retro-horaire.

1/4 de tour
-----------

=============== ==========
Points d'action Difficulté
=============== ==========
2 point         16
=============== ==========

La manœuvre consiste à faire tourner le navire de deux crans dans le sens horaire ou retro-horaire.

1/2 tour
--------

=============== ==========
Points d'action Difficulté
=============== ==========
3 point         40
=============== ==========

La manœuvre consiste à faire faire un demi-tour au navire dans le sens horaire ou retro-horaire.