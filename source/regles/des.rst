Dés
===


Les dés
-------

Le jeu n'utilise que des dés à 6 faces.

Un dé est noté 1D. Si il faut lancer 2 dés on note 2D, 3D pour 3, 4D pour 4 et ainsi de suite.

Les lancés de dés peuvent aussi avoir des points. Par exemple 2D+1 signifie que l'on lance 2D et que l'on rajoute 1 au
résultat obtenus.


Cassage de dés et points
------------------------

Dans certains cas (comme la création des fiches de personnages par exemple), les dés peuvent être cassés.

Le dés est soustrait et 3 points sont mis à disposition à la place.

Ex. : 3D = 2D + 1D = 1D+2 + 1D+1


Le dé du destin
---------------

Chaque action est soumise à un lancé de dé, et chaque lancé doit contenir un dé identifiale (différente couleur, taille,
…). Il s'agit du dé du destion.

Si le dé du destin vaux 1 alors quelquechose de mauvais arrive. Dans ce cas, il faut retirer le plus gros dé de la
somme du dé.

Si le dé du destin vaux 6 alors il faut fair la somme de tous les dés et relancer le dé du destin pour ajouter encore
une fois sa valeur, et ce tant que le dé du destin vaux 6.
