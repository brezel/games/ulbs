Matériel
========

Le jeu étant libre et uniquement téléchargeable, il appartient aux joueurs de se procurer et/ou de fabriquer le matériel
de jeu.

.. toctree::
   :maxdepth: 1

   plateau
   achat