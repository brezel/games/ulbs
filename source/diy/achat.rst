Liste d'achat
=============

.. important::

    La liste suivante est à titre indicatif. Elle fournis le matériel nécessaire pour pouvoir jouer au cas où vous ne
    souhaiteriez pas le fabriquer vous même.

    Je donne les liens vers Amazon US car ils ont plus de choix et sont moins cheres sur certains articles.

Plateau
-------

- Plateau blanc effacable - 35€ - https://www.amazon.com/Battle-Grid-Game-Mat-Reusable/dp/B019YLRP2G
- Plateau « vintage » effacable - 34€ - https://www.amazon.com/Battle-Grid-Game-Mat-Distressed/dp/B01MQHECUR

Pions
-----

- Pions blancs de 1" - - https://www.amazon.com/Value-Pack-50-Counters-Markers/dp/B01N98WGBG

Dés
---

.. todo Définir le nombre maximum de dés devant être jetés afin de préciser de combien de dés un joueur doit posséder

Vous pouvez trouver des dés, où que vous soyez. Ce ne sont que des dés à 6 faces standards. La seule exigence est qu'un
des dés soit clairement identifiable (le dé du destin).