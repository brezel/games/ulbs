Dirigeables
===========

Calculs
-------

.. image:: /_static/ellipsoid.png
   :align: center


Volume d'un ballon
~~~~~~~~~~~~~~~~~~

.. math::

    V = \frac{4}{3} \pi a b c


Surface d'un ballon
~~~~~~~~~~~~~~~~~~~

.. math::

    S = 4 \pi ( \frac{a^p b^p + a^p c^p + b^p c^p}{3} ) ^ \frac{1}{p}

.. math::

    p = 1.6075

.. warning::

    Cette formule est une approximation (marge d'erreur de 1%). Elle se base sur la formule de Knud Thomsen.


Masse pouvant être levée par le ballon
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Un aérostat tiens en l’air grâce à la poussée d’Archimède.

    «Tout corps immergé dans un fluide subit une force opposée au poids du fluide déplacé.»

.. math::

    \vec{F} = p \vec{g} V

:math:`\vec{F}` est la portance.